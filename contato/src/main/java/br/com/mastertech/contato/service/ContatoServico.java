package br.com.mastertech.contato.service;

import br.com.mastertech.contato.Contato;
import br.com.mastertech.contato.repository.ContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;

@Service
public class ContatoServico {

    @Autowired
    ContatoRepository contatoRepository;

    public void insereContato(Contato contato) {
        contatoRepository.save(contato);
    }

    public List<Contato> getContatos(Principal principal) {
        return contatoRepository.getByUsuario( Integer.parseInt(principal.getName()) );
    }
}
