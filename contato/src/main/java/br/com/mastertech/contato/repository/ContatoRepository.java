package br.com.mastertech.contato.repository;

import br.com.mastertech.contato.Contato;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ContatoRepository extends CrudRepository<Contato, Integer> {
    List<Contato> getByUsuario(Integer usuario);
}
