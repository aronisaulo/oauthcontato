package br.com.mastertech.contato;

import br.com.mastertech.contato.service.ContatoServico;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping
public class ContatoController {

    @Autowired
    ContatoServico contatoServico;

    @PostMapping("/contato")
    public void inserirContato (@RequestBody Contato contato){
        contatoServico.insereContato(contato);
    }

    @GetMapping("/contatos")
    public List<Contato> listaContato(Principal principal) {
        return contatoServico.getContatos(principal);
    }
}
